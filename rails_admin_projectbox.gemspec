$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rails_admin_projectbox/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rails_admin_projectbox"
  s.version     = RailsAdminProjectbox::VERSION
  s.authors     = ["ProjectBox, Lda"]
  s.email       = [""]
  # s.homepage    = "TODO"
  s.summary     = "ProjectBox theme for rails rails_admin"
  s.description = "This theme is meant to keep rails_admin looking sharp with the latest design directives."
  s.license     = "MIT"

  s.files = Dir["{app,lib,vendor}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.4"
end
